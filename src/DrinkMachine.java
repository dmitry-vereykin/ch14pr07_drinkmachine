/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class DrinkMachine extends JApplet {

    private JTextField priceField, coinsField, changeReturnField, canDropField, warningsField;
    private JButton[] sodaButtons;
    private String[] soda = {"Cola","Lemon-lime soda","Grape Soda","Root beer","Bottled water"};
    private int[] sodaLeft = {20, 20, 20, 20, 20};
    private final double price = 0.75;

    public void init() {
        setLayout(new BorderLayout());

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        priceField = new JTextField(5);
        priceField.setEditable(false);
        priceField.setText(" $" + price);
        topPanel.add(new Label("Price"));
        topPanel.add(priceField);

        coinsField = new JTextField(5);
        coinsField.setEditable(true);
        topPanel.add(new Label("   COIN SLOT:"));
        topPanel.add(coinsField);

        this.add(topPanel, BorderLayout.NORTH);

        JPanel sodaPanel = new JPanel();
        sodaPanel.setLayout(new GridLayout(3, 2));

        sodaButtons = new JButton[soda.length];

        for (int i = 0; i < soda.length; i++) {
            sodaButtons[i] = new JButton(soda[i]);
            sodaButtons[i].addActionListener(new ButtonListener());
            sodaPanel.add(sodaButtons[i]);
        }

        warningsField = new JTextField(10);
        warningsField.setEditable(false);
        warningsField.setHorizontalAlignment(JTextField.CENTER);
        warningsField.setForeground(Color.RED);
        sodaPanel.add(warningsField);

        this.add(sodaPanel, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());
        canDropField = new JTextField(10);
        canDropField.setEditable(false);
        bottomPanel.add(new Label("CAN SLOT:"));
        bottomPanel.add(canDropField);

        changeReturnField = new JTextField(5);
        changeReturnField.setEditable(false);
        bottomPanel.add(new Label("Coin Return:"));
        bottomPanel.add(changeReturnField);

        this.add(bottomPanel, BorderLayout.SOUTH);

    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double coins;
            double payment;
            double coinReturn;
            Object source = e.getSource();
            DecimalFormat df = new DecimalFormat("##0.00");

            try {
                coins = Double.parseDouble(coinsField.getText());

                while (true) {
                    if (coins >= price) {
                        payment = coins;
                        warningsField.setText("");

                        for (int n = 0; n < sodaButtons.length; n++) {
                            if (sodaButtons[n] == source) {

                                if (sodaLeft[n] == 0) {
                                    warningsField.setText("This item out of stock");
                                    coinsField.setText("");
                                    canDropField.setText("");
                                    changeReturnField.setText("");
                                    break;
                                }

                                canDropField.setText(soda[n]);
                                coinReturn = payment - price;
                                changeReturnField.setText(df.format(coinReturn));
                                sodaLeft[n] = sodaLeft[n] - 1;
                            }
                        }

                        break;
                    } else {
                        changeReturnField.setText(df.format(coins));
                        warningsField.setText("Not enough cash");
                        coinsField.setText("");
                        break;
                    }
                }

            } catch (Exception exc) {
                warningsField.setText("Insert cash first");
            }


        }
    }

}
